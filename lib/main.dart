import 'package:flutter/material.dart';

void main(){
  runApp(
    new MaterialApp(
      title: 'Menu Diya',
      home: new Home()
    )
  );
}


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Diya Demo"), backgroundColor: Colors.red[700],),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Deni Marswandi"),
              accountEmail: new Text("marswandi@gmail.com"),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: new NetworkImage("http://www.pngmart.com/files/7/Python-Transparent-Background.png"),
              ),
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new NetworkImage("https://image.freepik.com/free-vector/abstract-technology-particle-background_52683-25766.jpg"),
                fit: BoxFit.cover
                )
              ),
              otherAccountsPictures: <Widget>[
                new CircleAvatar(backgroundImage:new NetworkImage('https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582748_960_720.png'),)
              ],
            ),
            new ListTile(
              title: new Text("Settings"),
              trailing: new Icon(Icons.settings),
            ),
            new ListTile(
              title: new Text("Close"),
              trailing: new Icon(Icons.close),
            )
          ],
        ),
      ),
      body: new Container(

      ),
    );
  }
}